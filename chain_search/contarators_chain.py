from sql_queries import get_contractors_by


def find_contractors_chain(order_chain: list[dict]):
    '''
    Главная функция. На вход подается словарь order_chain. На выходе - список исполнителей.
    :param order_chain: входная цепочка заказов (шагов исполнения заказа клиента)
    :return: list[Contractor] - список исполнителей по порядку индексов заказов
    '''
    # TODO
    pass


def find_contractor(order: dict):
    '''
    Ищет наилучшего исполнителя для заказа (этапа исполнения заказа клиента)
    :param order: заказ
    :return: исполнитель
    '''
    # TODO
    # Получение из sql списка подходящих исполнителей
    contractors = get_contractors_by()
    # Отсев по времени
    # Рассчет quality исполнителя
    # Выбор исполнителя на основании quality_coefficient и price_coefficient
    contractor = None
    return contractor


def time_filter(contractors: list[dict], time_from, time_to):
    '''
    Отсеивает иполнителей не подходящих по графику работы.
    :param contractors: список исполнителей
    :param time_from: время начала исполнения заказа
    :param time_to: время окончания исполнения заказа
    :return: отфильтрованный список исполнителей
    '''
    pass
