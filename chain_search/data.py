class Geozone:
    def __init(self, id: int, name: str):
        self.id = id
        self.name = name


class ServiceType:
    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name


class Service:
    def __init__(self, id: int, name: str, description: str, service_type: ServiceType):
        self.id = id
        self.name = name
        self.description = description
        self.service_type = service_type


class SupplementaryCondition:
    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name


'''class Contractor:
    def __init__(self, id: int, name: str, services: list[Service],
                 supplementary_conditions: list[SupplementaryCondition], statistics: dict):
        self.id = id
        self.name = name
        self.services = services
        self.supplementary_conditions = supplementary_conditions
        self.statistic
'''