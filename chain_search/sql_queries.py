from datetime import datetime


def get_contractors_by(service_id: int, geozone_id: int, time_from, time_to, supplementary_conditions: list):
    '''

    :param service_id: id услуги
    :param geozone_id: id геозоны доступной для исполнения данной услуги у исполнителя
    :param time_from: начало временного интервала, который должен входить в график работы исполнителя
    :param time_to: конец временного интервала, который должен входить в график работы исполнителя
    :param supplementary_conditions: доп. услуги выполняемые исполнителем вместе с данной основной услугой
    :return: список подходящих исполнителей
    '''
    # TODO
    # Исполнители не отфильтрованные по времени
    initial_contractors = sql_query(service_id, geozone_id, supplementary_conditions)
    contractors = []
    for contractor in initial_contractors:
        contractor_time_from = datetime.strptime(contractor['provided_from'], '%h%h:%m%m')
        contractor_time_to = datetime.strptime(contractor['provided_to'])

    return contractors


def sql_query(service_id: int, geozone_id: int, supplementary_conditions: list, weight: int):
    '''

    :param service_id: id услуги доступной для выполнения испонителем
    :param geozone_id: id геозоны доступной для исполнения данной услуги у исполнителя
    :param supplementary_conditions: доп. услуги выполняемые исполнителем вместе с данной основной услугой
    :param weight: вес груза. Исполнитель, для которго данный вес выходит за рамки допустимого, не подходит.
    :return: список словарей подходящих под критерии исполнителей в формате (не указанные поля опустить): [{
                'id': int,
                'service_price': int, (цена основной услуги)
                'supplementary_price': int, (цена доп. услуги)
                'average_execution_time': int,
                'provided_from': str,  (временной интервал доступный
                'provided_to': str      для выполнеия данной основной услуги)
             }]
    Если что не понятно по формату, можно в дискорде обсудить.
    '''
    # TODO: Khan
    contractors = []
    return contractors
